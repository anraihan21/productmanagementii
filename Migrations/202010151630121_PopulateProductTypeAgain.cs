namespace ProductManagementII.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateProductTypeAgain : DbMigration
    {
        public override void Up()
        {
            Sql("Insert Into Producttypes (Id, Name) Values (2 , 'Electronics')");
            Sql("Insert Into Producttypes (Id, Name) Values (3 , 'Software')");
            Sql("Insert Into Producttypes (Id, Name) Values (4 , 'Service')");
            Sql("Insert Into Producttypes (Id, Name) Values (5 , 'Gadgets')");

        }
        
        public override void Down()
        {
        }
    }
}
