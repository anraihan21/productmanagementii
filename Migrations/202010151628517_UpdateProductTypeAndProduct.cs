namespace ProductManagementII.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductTypeAndProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ExpDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "NumInStock", c => c.Int(nullable: false));
            DropColumn("dbo.ProductTypes", "ExpDate");
            DropColumn("dbo.ProductTypes", "Price");
            DropColumn("dbo.ProductTypes", "NumInStock");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductTypes", "NumInStock", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTypes", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTypes", "ExpDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Products", "NumInStock");
            DropColumn("dbo.Products", "Price");
            DropColumn("dbo.Products", "ExpDate");
        }
    }
}
