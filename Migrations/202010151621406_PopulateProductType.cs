namespace ProductManagementII.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Web.Mvc.Ajax;

    public partial class PopulateProductType : DbMigration
    {
        public override void Up()
        {
            Sql("Insert Into Producttypes (Id, Name, ExpDate, Price, NumInStock) Values ( 1, 'Grocery', '8/8/2020', 500, 10)");
        }
        
        public override void Down()
        {
        }
    }
}
