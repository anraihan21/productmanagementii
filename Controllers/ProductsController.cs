﻿using ProductManagementII.Models;
using ProductManagementII.ViewModels;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductManagementII.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext _context;

        public ProductsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        public ViewResult Product()
        {
            var products = _context.Products.Include(c => c.ProductType).ToList();
            return View(products);
        }
        public ActionResult Details(int id)
        {
            var product = _context.Products.Include(c => c.ProductType).SingleOrDefault(c => c.Id == id);

            if (product == null)
                return HttpNotFound();

            return View(product);
        }
    }
}