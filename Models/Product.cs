﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManagementII.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ExpDate { get; set; }
        public int Price { get; set; }
        public int NumInStock { get; set; }
        public ProductType ProductType { get; set; }
        public byte ProductTypeId { get; set; }
    }
}