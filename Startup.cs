﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProductManagementII.Startup))]
namespace ProductManagementII
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
