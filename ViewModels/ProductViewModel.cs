﻿using ProductManagementII.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManagementII.ViewModels
{
    public class ProductViewModel
    {
        public List<Product> Products { get; set; }
    }
}