﻿using ProductManagementII.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManagementII.ViewModels
{
    public class CustomerViewModel
    {
        public List<Customer> Customers { get; set; }
    }
}